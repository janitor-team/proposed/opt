2003-03-18  James Theiler  <jt@yks.lanl.gov>

	* opt.texi: Note that "%%" on the command line is followed
	by an exit.

	* src/opt_proc.c (opt_process): changed behavior of opt. Now
	writing "program %%" will produce program.opt and quit.  In
	the menu, however, it won't exit.

	* src/opt_menu.c,opt_proc.c,opr_reg.c,opt_p.h: changed menuflag
	to opt_menuflag.

2003-03-14  James Theiler  <jt@yks.lanl.gov>

	* test/manyopts.c (main): free all those strdups.

	* src/opt_util.c (optAbortRun): calls opt_quit() when
	it quits.

	* src/opt_proc.c (opt_lineprocess): free memory allocated
	in line2argv
	(l2a): set argv[i] to strdup(argv[i]) so that the line is
	freeable.
	(opt): free the optlist array when finished;
	uses new function free_optlist() so that optlist needn't be
	made globally available.

	* src/ag.c (ag_prepend_argv0): now does not strdup the strings
	when it prepends.	

2003-03-13  James Theiler  <jt@yks.lanl.gov>

	* ext/Opt.pm: added --version 

2003-03-13  James Theiler  <jt@heretic.nambeInstitute.org>

	* src/opt_reg.c (opt_tofile): now uses justification
	when writing out the opt file.
	(opt_fprint): removed, obsolete.

2003-03-12  James Theiler  <jt@heretic.nambeInstitute.org>

	* src/opt_reg.c, src/opt_menu.c: fixed various sprintf's so
	that snprintf would be used instead; this is done using
	opt_snprintf_N(buf,n,fmt,v1,...,vN) for N=1,2,3,4.

	* src/opt_p.h (opt_fatal_*, opt_warn_*): now setup to use
	snprintf instead of sprintf if it is available; this should
	prevent coredumps at least...it will still truncate error
	messages.

	* configure.in: check for snprintf

	* test/checkopt: add a test that causes a coredump if snprintf
	is not used, but the old sprintf is used instead.

2003-02-06  Steven Brumby  <brumby@lanl.gov>

	* Makefile.am, configure.in, opt-RPM.spec.in, opt.spec.in, makePRM.in:
	You can now build an RPM.  opt-RPM.spec.in and makeRPM.in 
	have been replaced by opt.spec.in, and Makefile.am and 
	configure.in are updated. See NEWS for RPM build instructions.
	
2002-11-13  Simon Perkins  <s.perkins@lanl.gov>

	* src/opt_proc.c: Fixed bug in opt_process() that meant that a
	positional argument '-' was not processed correctly.

2002-11-04  Simon Perkins  <s.perkins@lanl.gov>

	* src/makeopth.pl: Changed generation of opt_regcc.cc so that
	redundant default argument values for OptRegister() are no longer
	included in the function _definitions_. They are still included in
	the declarations in opt.h of course.

2002-09-19  James Theiler  <jt@heretic.nambeInstitute.org>

	* test/checkopt: had to alter testlong's grepstring since
	changing OPTDESCRIPTWIDTH reformatted things slightly.

	* src/opt_reg.c: removed opt_menu_enabled; a superfluous 
	variable which caused a warning message when opt.c was
	generated because opt_menu_enabled also appears as a static
	int in the opt_proc.c 
	(OPTDESCRIPTWIDTH): changed from '41' to '43' which enables
	descriptions to just fit on an 80 column line.  I suppose
	if we were *too* smart, we could make this width a variable,
	and adjust it according to the terminal characteristics.

2002-09-17  Simon Perkins  <s.perkins@lanl.gov>

	* Added an optProgName() function to allow the programmer to
	explicitly set the program name.

	* Added an optPrintUsage() function to allow programmer to
	explicitly request that the usage message be printed.

2002-09-16  Simon Perkins  <s.perkins@lanl.gov>

	* Added an optregf() function to register flexible options.

	* Extended C++ interface to allow setting of positional and
	flexible options using C++ API.

	* Updated documentation.

2002-09-13  James Theiler  <jt@heretic.nambeInstitute.org>

	* src/opt_reg.c (opt_usage): fussed around until descriptions 
	all lined up, descriptions that follow long option names start
	on the next line. (opt_write_xml_file): added mode and array
	delimiter.

2002-09-13  James Theiler  <jt@yks.lanl.gov>

	* opt.texi: updated docs

	* test/checkopt: tests the new tricks that tarray does.  also,
	added a new test to testtypes.

	* test/tarray.c (main): does new tricks to stretch array options;
	invokes optarraydelim, and new function optregp_array.  also,
	a tricky hook function to set delimiter on the fly.

	* test/testtypes.c: initialized lvstring so it has a default
	value.

	* src/opt_reg.c (opt_number): fixed bug that caused it to fail for
	array options, it was using optlist[n].value instead of
	optlist[n].array->value, and it is the latter that the user sees.
	(optarray_getstrval): takes extra argument, delimiter.
	(OptArray_t): removed sdelim attribute.
	(optregp_array): added new function, extending Simon's optregp
	to array variables.

	* src/opt.h, src/opt_reg.c: (optarraydelim_n,optarraydelim): added
	new function to set array delimiter to something besides comma.

	* src/opt_proc.c: removed opt_pkg, unusued.

2002-09-12  Simon Perkins  <s.perkins@lanl.gov>

	* test/checkopt: Opt now interprets bare command characters, such
	as '.', '=' and '!' as positional parameters if the menu is not
	currently being invoked. The '.' in particular is a common
	positional parameter. Tests added to check for this.

	* opt.texi: Updated docs to reflect new positional options.

	* Opt now has the notion of positional options as an orthogonal
	concept to option type, handled by a mode field in the Option_t
	structure. Previously, you were limited to positional string
	options via the UNDELIM type, now you can have positional
	parameters of arbitrary type. You can also have options that can
	be set either positionally or via the delimited syntax. These are
	called flexible options.

	* The OPT_UNDELIM and OPT_UNDELIMC types have been deprecated.
	
2002-09-10  Simon Perkins  <s.perkins@lanl.gov>

	* src/opt_reg.c: The usage message now separates out undelimited
	(positional arguments) from delimited options.

	* opt.texi: Added some documentation about default values.

	* src/opt_reg.c: Undelimited string options are no longer set to null
	upon registering. Handling is now consistent with named string options.

2002-09-10  James Theiler  <jt@yks.lanl.gov>

	* opt.man: removed, it is obsolete.

2002-09-09  Simon Perkins  <s.perkins@lanl.gov>

	* opt.texi: Updated docs to mention optDisableMenu().

	* test/checkopt: Added tests for long option handling and menu
	disablement.

	* src/opt_reg.c: opt_justify() is used to pretty-print the
	description strings in both the usage message and the menu. It
	could also be used to pretty print the description that is written
	to options files, but this has not been done yet.

	* src/opt_util.c: Added opt_justify() function, which takes
	arbitrary text and justifies it using a user supplied column width,
	indent, intitial leading and optional prefix. This is used in
	opt_usage() to pretty-print the description strings.

	* src/opt_proc.c: Added optDisableMenu function. If invoked, this
	leads to a warning if the --menu option is used (and the menu does
	not appear). It also removes the --menu message from the usage
	string.

2002-09-04  James Theiler  <jt@yks.lanl.gov>

	* configure.in: upped version to 3.15

2002-08-23  Simon Perkins  <s.perkins@lanl.gov>

	* Remerged opt.h and optcc.h into a single file again. The C++
	interface can be optionally disabled for C++ programs by defining
	the symbol NOLIBOPTCC.

2002-08-22  Simon Perkins  <s.perkins@lanl.gov>

	* Merged liboptcc.a into libopt.a. liboptcc.a now obselete.

	* We now link against a single library, libopt.a for both the C
	and C++ interfaces. To enable both the C and C++ interfaces, we
	include optcc.h; to enable just the C interface, we include opt.h.

2002-08-16  Simon Perkins  <s.perkins@lanl.gov>

	* Optional C++ now functional, but seems a little cumbersome. Will
	probably rewrite to have separate opt.h and optcc.h headers.

	* src/Makefile.am: Added configure logic to specify if user wants
	C++ (by default disabled). Currently this just defines ENABLE_CXX
	but does not do anything else yet.

2002-04-22  James Theiler  <jt@yks.lanl.gov>

	* configure.in: upped version number to 3.14

	* test/checkopt: added a check for the most complicated case,
	the uninitialized string.

	* test/tstring.c (main): made it so that it works on solaris;
	actually, there is no reason it should have work on linux either!

	* src/Makefile.am: 'ln' to 'ln -f' because sometimes the
	hard link target already exists.

	* src/opt_p.h (opt_mess_*): changed opt_mess macros to call
	printf directly instead of sprintf to a fixed length string,
	which was then an argument to opt_message.  Only disadvantage
	if is opt_message changes its policy in the way it writes, then
	the macros will also be changed.  Also increased the size of
	the fixed strings that opt_warn and opt_fatal use.

	* src/{opt_proc.c, opt_menu.c, opt_p.h}: Removed "flag" type; I
	didn't use it that much, and it threatened to conflict with other
	"flag" names or types.

	* src/opt_proc.c (short_usage): by including opt_usagestring
	as an argument to opt_mess_*() instead of opt_message, I was
	limiting the length of the using string to a fixed value.
	The *real* fix will be to fix the opt_mess macros.

2002-04-19  James Theiler  <jt@yks.lanl.gov>

	* src/Makefile.am: modified to build a new library, liboptcc.a,
	which works with C++ programs

	* test/Makefile.am: modified to use liboptcc.a
	instead of its own optcc.o

2002-03-17  James Theiler  <jt@heretic.nambeInstitute.org>

	* src/opt_proc.c (opt_process): added --xml-opt-interface option
	* src/opt_reg.c: added functions: opt_write_xml_file() and some
	support functions, such as opt_xml().
	* src/opt_p.h: added prototype for opt_write_xml_file()

2001-11-17  James Theiler  <jt@hiris-sun.scheme.nis.lanl.gov>

	* src/opt_reg.c: "Unknown" to opt_reason_unknown since
	compiler complained about const char * and char *

2001-09-30  James Theiler  <jt@heretic.nambeInstitute.org>

	* src/opt_reg.c (optdeprecate): just cleaned up a little, 
	to satisfy the fussy compiler.

	* test/Makefile.am, src/Makefile.am (CLEANFILES): now needed 
	for the latest automake's version of 'make distcheck'

2001-09-10  Damian Eads    <eads@lanl.gov>

        * src/opt.h: added necessary prototypes for optdeprecate.
        * src/opt_menu.c: added deprecation information. Started to
          implement the ability to get option properties within the menu
          system.
        * src/opt_p.h: added character which represents the "get info" 
          command for the menu system.
        * src/opt_reg.h: added optdeprecate. Changed opt_action,
          opt_usage to display deprecation information.
	* test/testdep.c: added test for deprecation.

2001-09-08    <jt@lunatic.nambeInstitute.org>

	* GNUmakefile (ftprelease): scp to mrfrench

	* src/opt_proc.c (optVersion): sets a version number which
	is used so that if the user runs 'program --version' then
	then "program: Version XX.X" is printed, and the program
	exits.

	* test/checkopt, test/testopt.c (main): added test for 
	the builtin --version code.

	* src/opt.h: some reformatting of the optregX_array() 
	prototypes since I consider opt.h part of the documentation.

	* src/opt_proc.c: made a lot of global opt_XXX variables 
	static to this function, since they are only used here.
	renamed optfile_name to opt_filename, for consistency.

	* src/opt_sf.h: removed the '#ifdef for convex and _TURBOC_,
	assuming those platforms are either obsolete, or that their
	compilers have improved.

	* src/Makefile.am, src/opt_proc.c (opt_process): renamed VERSION
	to OPT_VERSION, and PACKAGE to OPT_PACKAGE, so that opt.c can be
	included in other packages.

2001-09-07    <jt@lunatic.nambeInstitute.org>

	* src/Makefile.am: removed src/t_num since it is moved to test
	directory

2001-09-06    <jt@lunatic.nambeInstitute.org>

	* test/t_num.c: moved from src/t_num.c so that the test can be 
	formalized in test's 'make check'

	* test/checkoptcc: new script that works like checkopt but runs
	on testcc to check the C++ capabilities of opt.

	* test/Makefile.am: added checkoptcc and testcc and a way
	to build testcc, even when libopt.a has already been compiled
	under C

	* src/Makefile.am: t_num is now run under 'make check'

	* src/t_num.c (main): generally cleaned up so that it could
	be part of the formal 'make check'

	* src/opt_reg.c (optarray_getstrval): had do so some arcane
	casting to get ANSI C++ compatibility.  actually, in retrospect
	it all makes sense, but read the comments in this function 
	for further details.

2001-09-04  James Theiler  <jt@yks.lanl.gov>

	* test/checkopt (output): thanks to Michael Tempsch for
	pointing out that the problem with checkopt wasn't the
	trailing right brackets but the lack of a semicolon.

2001-08-29  James Theiler  <jt@yks.lanl.gov>

	* test/checkopt (checkit,checkitneg,waserror): solaris
	wanted the trailing right brackets on their own line.

2001-08-28    <jt@lunatic.nambeInstitute.org>

	* src/opt_reg.c (optarray_action): brand new function does
	the processing of array options.  It is called from the
	opt_action() function if the opt_TYPE is OPT_STRING and
	if the array attribute is non-null.

	* src/opt_reg.c (opt_CSTRING_deprecated): writes a warning
	whenever CSTRING's are used.
	(optrega_array): when registering arrays, now checks that
	the base type is appropriate -- you cannot for instance
	register an array of INTLEVEL's -- you also cannot register
	an array of ABSFLAG's but maybe you should be able to?

	* src/opt_reg.c: renamed ARRAY_OPT to be OptArray_t and
	EACH_ARRAY to be Option_t.

	*src/opt_reg.c (OPT_GETVALUE, OPT_SETVALUE, OPT_GET_ITH_VALUE,
	OPT_SET_ITH_VALUE): renamed a number of macros to be clearer,
	I hope, and more consistent with each other.  The first two
	take `void *' pointers and get and set values associated with
	those pointers based on the opt_TYPE.  The last two do the
	same thing, but use an index to the optlist[] array as an
	argument, with optlist[].value being the `void *' pointer of
	interest.

	And similarly for a bunch of functions.  Some use the `void *' as
	an argument, and some use an index to the optlist[] as an
	argument.  In some cases, all you need is the `void *' -- eg,
	getting and setting of simple numeric values -- but in some cases
	extra processing is needed, like the FLAG's and INTLEVEL types.
	Thus the functions that take an index as an argument will for some
	types be able to farm out the work to the version of the function
	that takes a `void *' pointer argument, and for other types have
	to do some more processing using other components of the Option_t
	structure.  For example, the old function optstrval, which converted
	an index into a string corresponding to the value of the option,
	now calls opt_getstrval() for the simpler options.  Meanwhile,
	the newfangled array processing uses opt_getstrval() to get
	strings for individual elements of an array.

	* test/testtypes.c: no longer force an error using CSTRING.
	This is, the test code now uses them properly, even though
	not using them at all is the proper thing to do.

	* test/tarray.c: now tests arrays of strings as well
	as the arrays of doubles it did before;  
	* test/checkopt: employs this test of strings. 

	* src/opt_p.h: removed some prototypes and replaced them
	with static functions in opt_reg.c

2001-08-28  James Theiler  <jt@yks.lanl.gov>

	* test/checkopt: added check that the array string survives
	at least until the menu is invoked.  this check is what revealed
	the scope error below.

	* src/opt_reg.c (optrega_array): very subtle scope error; the
	string variable being registered with optrega() goes out of 
	scope when optrega_array returns.  fixed this with a static
	array of string variables.
	(optstrarrv): added this function to allocate and write the 
	string associated with the values in an array variable.

2001-08-27  James Theiler  <jt@yks.lanl.gov>

	* src/opt_reg.c: oops, the size attribute to the ARRAY_OPT struct
	really does need to be an array.  I was mistaken to change it.
	(optrega_array): oops again: I had changed the second argument from
	void * to void **, and now realize that was a mistake, because 
	although it works, the compiler complains when i pass, eg, an int **.
	so i renamed the arg a void *, but now cast it as void ** after it
	is passed.

2001-08-25    <jt@lunatic.nambeInstitute.org>

	* src/opt_reg.c: "refactor" Damian's code:
	(ARRAY_OPT) value is now a void **, not a void *
	(ARRAY_OPT) size is now an int, not an int *
	(EACH_OPT) now has a pointer to ARRAY_OPT structure, instead of
	  a flag and an actual ARRAY_OPT structure; if the pointer is NULL
	  then the option is normal, if it is an array then it points to
	  an ARRAY_OPT struct with the new fields appropriately populated
	(optreg_array) by recognizing that val is really a void **, and
	  not a void *, and that you are allowed to dereference a void **,
	  this code could be considerably simplified.  it is also more
	  stable against memory leaks.  and of course it has been modified
	  to work with the new structure in EACH_OPT/ARRAY_OPT	
	(optstrtyp) modified so that --help says "<basetype ARRAY>" 
	  instead of "<STRING ARRAY>" 
	(optarraycast) i did not modify this function, but i enclosed
	  it within an #if 0 / #endif because i am pretty sure it is 
	  unnecessary.
	(optrega) modified the way opt_nreg is incremented

2001-08-25    <jt@lunatic.nambeInstitute.org>

	* removed all the files from cvs that are generated automatically;
	configure and all the Makefile's and Makefile.in's

	* test/testtypes.c: I think this was done awhile ago but an
	assignment of a CSTRING is now done with an adaptive array []
	instead of a fixed [MAXOPTSTRLEN] array, and this leads to an
	error in 'make check'
	
	* test/checkopt (waserror): added an extra CR/LF so that 
	real errors would stand out more.  also added manyopts 
	and tarray to the list of executables that are made.
	Also, noted that CSTRING error is a known error.

2001-08-24  Damian Eads    <eads@lanl.gov>

	* configure: generated again.
	* src/Makefile
	* src/opt.h: added necessary definitions and structures for arrays.
	* src/opt_p.h: added necessary prototype definitions for opt_reg
	to process arrays.
	* src/opt_reg.c: added necessary code to register array options.
	* test/Makefile: autogenerated.
	* test/Makefile.am: added entry for testarray.
	* test/Makefile.in: autogenerated.
	
2001-08-17  James Theiler  <jt@yks.lanl.gov>

	* GNUmakefile: replaced gpl-dist and lgpl-dist with lanl-dist
	to include the LANL COPYING statement in all the distributed
	source code.

	* opt.texi (Registering options): added paragraph on the C++
	OptRegister() function

2001-08-16    <jt@lunatic.nambeInstitute.org>

	* src/opt_sf.h: fixed the COPYING statement in the header.

	* opt/opt.texi: updated documentation

2001-08-16  James Theiler  <jt@yks.lanl.gov>

	* src/opt_reg.c (optrega): check that the longname doesn't 
	include spaces -- easy mistake to make if you've put the
	brief description in as the longname

	* src/opt_num.c: needed to enclose includes inside an #ifndef 
	SINGLEFILE directive.

2001-08-15    <jt@lunatic.nambeInstitute.org>

	* src/opt_reg.c (opt_action): now makes calls to one of opt_atof,
	opt_atoi, or opt_atou, depending on type; this way warning
	messages can be tailored to datatype -- ie, invalid number,
	invalid integer, invalid positive integer.

	* src/opt_num.c: new function opt_isvalidnumber, along with the
	new suite of opt_atof, opt_atoi, and opt_atou which replace
	opt_numeric, which in turn replaced argnextnum.

	* src/t_num.c: new file; for now, an informal test of the
	opt_num.c functions.

	* test/checkopt: more new tests for validity of numbers,
	integers, and unsigned integers

2001-08-15  James Theiler  <jt@yks.lanl.gov>

	* test/checkopt: added a check that invalid numeric arguments be
	flagged

	* src/opt_num.c, src/Makefile.am: new file, provides opt_numeric()
	which replaces old argnextnum() from ag.c checks for whether a
	number is valid before calling atof().  eventually, should check
	for integer/float/signed, but for now just checks for numeric.

	* src/ag.c, src/ag.h: removed argnextnum().

	* src/opt_p.h: added opt_numeric().

	* src/opt_reg.c: replaced argnextnum(ag) with opt_numeric(argnext(ag))

2001-08-14    <jt@lunatic.nambeInstitute.org>

	* test/tnull.c, test/tcheck.c: new tests
	
	* test/checkopt: added new checks.  tnull in particular
	checks that the test for the val==NULL bug works.

	* test/Makefile.am: added tstring and tnull to the list
	of programs that test opt features.

	* src/opt_reg.c (optrega): added check for val==NULL 
	which should not be allowed in call to optreg functions.
	

